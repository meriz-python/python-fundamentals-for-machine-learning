#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 10 3 2021
#UpD:
#Descri: learning basics of cleaning and importing data. Demo 3

### Read Clean Explore DataSets

# Resources

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

# Imports
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score

## Data Load

automobile_df = pd.read_csv('datasets/cars_processed_08032021.csv')

print_full(automobile_df.head())
print(automobile_df.shape) #387, 8

## Regression

# regression w one feature
X = automobile_df[['Age']]
Y = automobile_df['MPG']

# plot so you can see any relationship between age and milage
fig, ax = plt.subplots(figsize = (12, 8))
# plt.scatter(automobile_df['Age'], automobile_df['MPG'])
plt.xlabel('Age')
plt.ylabel('Mpg')
# plt.show()

# use age to predict mileage
# using sklearn, model selection and train test splitting
# this only splits the data, does no training!
# 20% is for testing (80-20 split)
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)

# use LinearRegression to perform the actual learning
# high-level API.
# normalize to true, X-features normalized before regression is done
# normalize = x - mean / L2Norm
linear_model = LinearRegression(normalize=True).fit(x_train, y_train)

# Evaluate regression

# training score
# this is the r-sq score: 0.3367484844657359
print("Training score: ", linear_model.score(x_train,y_train))
# this is the r-sq score

# for prediction
y_pred = linear_model.predict(x_test)

# testing score
# r-sq score on the test data: 0.2612256977660745
print("Testing score: ", r2_score(y_test,y_pred))

# notes:
# our model has an OVERFIT on the training data. the 2 scores are near same
# and the testing score is really low.


# Plotting the Regression

# plt.scatter(x_test, y_test)
# plt.plot(x_test, y_pred, color = 'r')

plt.xlabel('Age')
plt.ylabel('Mpg')
# plt.show()

## Regression part 2

X = automobile_df[['Horsepower']]
Y = automobile_df['MPG']

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)

linear_model = LinearRegression(normalize=True).fit(x_train, y_train)

# Training score: 0.5740574864856356
print("Training score: ", linear_model.score(x_train,y_train))

y_pred = linear_model.predict(x_test)

# Test score:  0.5970684291900352
print("Test score: ", r2_score(y_test, y_pred))

# notes:
# with horsepower as a feature, model performs better
# training score is higher  .57 and testing score is higher .597
# suggests horespower feature has more predictive power for milage compared to the age

# Plotting the Regression

# original scatter
# plt.scatter(automobile_df['Horsepower'], automobile_df['MPG'])
plt.xlabel('Horsepower')
plt.ylabel('Mpg')
# plt.show()

# plt.scatter(x_test, y_test)
# plt.plot(x_test, y_pred, color = 'r')
#
# plt.xlabel('Horsepower')
# plt.ylabel('Mpg')
# plt.show()

## Regression part 3: Multiple regression

# based on the data, we may want to drop a few features that are non-numeric
# we may also use one-hot encoding on discrete or non continuous data types
print_full(automobile_df.head())

# get_dummies is one-hot encoding.
automobile_df = pd.get_dummies(automobile_df, columns=['Origin'])
print_full(automobile_df.head())

# multiple regression with all but MPG feature,
# b/c it is the target we want to predict
X = automobile_df.drop('MPG', axis =1)
Y = automobile_df['MPG']

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)

linear_model = LinearRegression(normalize=True).fit(x_train, y_train)

# Training score:  0.7951988583381087
print("Training score: ", linear_model.score(x_train,y_train))

y_pred = linear_model.predict(x_test)

# Testing score:  0.8293833196289753
print("Testing score: ", r2_score(y_test, y_pred))

# notes
# this is a better model than those w a single feature.
# train score is high
# test score is high
