#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 23 3 2021
#UpD:
#Descri: learning basics of imputing. Demo 7

### Read Clean Explore DataSets

# Resources

# prefunctions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

## Imports
import pandas as pd
import numpy as np

# tools for imputation
from sklearn.impute import SimpleImputer

# tools for pipeline
from sklearn.pipeline import make_pipeline
from sklearn.compose import ColumnTransformer

# tools for train test splitting
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

# evaluating predictions
from sklearn.metrics import accuracy_score


# Step 1;
# load data for the classification model
# this data has no missing values (it was cleaned)
# we will corrupt it slightly
diabetes = pd.read_csv("datasets/diabetes_processed.csv")
print_full(diabetes.head(10))

# Step 2:
diabetes_features = diabetes.drop('Outcome', axis =1)
diabetes_label = diabetes['Outcome']

print_full(diabetes_features.head())

# Step 3: let us randomly introduce missing values to this data set
# use NP.random
# mask only on the diabetes features
# this gives the same shape with random integers b/w 0-100.
# for a boolean mask, all nonzero = true, zero -> false
# nearly 1/100 values will be 0, ie false
mask = np.random.randint(0, 100, size=diabetes_features.shape).astype(np.bool)
# this will invert the boolean values of the provided mask above
# this means 1/100 will be true now. which we use to mark as missing
mask = np.logical_not(mask);

# Step 5: contaminate your data
# all 'true' values replaced with nans
diabetes_features[mask] = np.nan
print("Missing data introduced/some values masked")
print_full(diabetes_features.head(15))

# Step 6: import some tools see above
# these are imputations, pipelines, and classifiers/regressors

# Step 7:
# apply train test split function
# features, labels(outcomes), test size = 20%
x_train, x_test, y_train, y_test\
    = train_test_split(diabetes_features, diabetes_label, test_size=0.2)

# Step 8: transform data to impute missing values
# instantiate the column transformer object
# col transformer, apply sequence of transformations to the data
# on the features
# apply the simple imputer w the strategy = mean, missings will be set to column mean
# [0, 1, ..., 7] asks what columns to apply this transformation to
transformer = ColumnTransformer(
    transformers=[('features', SimpleImputer(strategy='mean'), [0, 1, 2, 3, 4, 5, 6, 7])]
)

# Step 9: create sci kit learn pipeline
# instantiate the make_pipeline object
# using the transformer object and a decision tree classifier
# this is your classifier object
clf = make_pipeline(transformer, DecisionTreeClassifier(max_depth=4))
# call the fit method of this classifier. this trains the model
clf = clf.fit(x_train, y_train)
# evaluate the fit on the training data
# nearly 80% of models predictions were correct <- on the training data.
print("Step 9: fit on training data")
print(clf.score(x_train, y_train))

# Step 10: use model on test data
# predict on test data
y_pred = clf.predict(x_test)
# calculate accuracy score
# Step 10: accuracy of preds
# 0.7467532467532467
# around 75% accuracy
print("Step 10: accuracy of preds")
print(accuracy_score(y_pred, y_test))





