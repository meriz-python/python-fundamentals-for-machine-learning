# Imports
import pandas as pd
import numpy as np
import datetime

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

##  Read In Data

# about cars data set: was modified and purposefully has missings
# etc for this module exercise
automobile_df = pd.read_csv('datasets/cars.csv')

# headers
print_full(automobile_df.head(5))

## Data Exploration
#Shape
print(automobile_df.shape)

# Missing Data:

# Replace '?' or any missing data character with NaNs
# ? marks are missing values
automobile_df = automobile_df.replace('?', np.NAN)
#check again to see if this is done.
# print_full(automobile_df.head(5))

# Check number of NaNs
print("Missing Values: ")
print(automobile_df.isna().sum())

# Now Replace Missing Vals w Current Mean (this is one technique)
automobile_df['MPG'] = automobile_df['MPG'].fillna(automobile_df['MPG'].mean())
print("Missing Values (after mean-fill MPG field): ")
print(automobile_df.isna().sum())

# Or Drop All records with missing fields
automobile_df = automobile_df.dropna()
print("How many records dropped with missing fields? ")
print(automobile_df.shape)

# Check for Null Values
# null values are deemed to be missing
print("Check for Null Values: ")
print(automobile_df.isnull().sum())

# Examining Columns w Judgment:

# Dropping a column
# note: might do this for / pre regression analysis
automobile_df.drop(['Model'], axis=1, inplace=True)
print_full(automobile_df.sample(5))

# also drop other columns/multiple columns at once
automobile_df.drop(['bore', 'stroke', 'compression-ratio'], axis=1, inplace=True)
print_full(automobile_df.head())


## Formatting Data

# Note: some of this data was not properly formatted at the beginning. Would be nice to make it so....
# gives numeric and nonnumeric values
print("Is Numeric Counts for Year")
print(automobile_df['Year'].str.isnumeric().value_counts())
#there are 36 false numerics

# now look at all any places in column where isNumeric equals false
# these values contin numeric years, and possibly other characters
print_full(automobile_df['Year'].loc[automobile_df['Year'].str.isnumeric() == False])

# extract run regular expression to extract first four characters of year column
extr = automobile_df['Year'].str.extract(r'^(\d{4})', expand = False)
print("Head after extracting first 4 characters in 'Year': ")
print(extr.head())

# check to see if year field has missing vals
print("Missing values: ", automobile_df['Year'].isnull().values.any())

# now reassign cleaned up data to the 'Year' column
# conversion to numeric necessary because extr has 'characters'
automobile_df['Year'] = pd.to_numeric(extr)
print("Data Type of year col: ", automobile_df['Year'].dtype)
print("Data after cleaning the year col: ")
print_full(automobile_df.head())

# Transform a column into something more familiar.
# year may be nice, but could be better to represent by age
automobile_df['Age'] = datetime.datetime.now().year - automobile_df['Year']
# now in place drop 'Year'
automobile_df.drop(['Year'], axis=1, inplace=True)
print_full(automobile_df.sample(5))

# Further cleaning: data types
print("Data Types of automobiles: \n", automobile_df.dtypes)

# now deal w Cylinders so it's not an object type
print("Nonnumeric counts Cylinders: \n", automobile_df['Cylinders'].str.isnumeric().value_counts())

# what do the false types look like?
#they are all dashes
print_full(automobile_df['Cylinders'].loc[automobile_df['Cylinders'].str.isnumeric() == False])

# For the non-numeric data types, we will replace w mean, but requires one quick step
# Step 0: calculate. only non numerics are the - char
cylinder_num_only = automobile_df['Cylinders'].loc[automobile_df['Cylinders'] != '-']
cylinder_mean = cylinder_num_only.astype(int).mean()
# Step 1: replace (not done in place)
automobile_df['Cylinders'] = automobile_df['Cylinders'].replace('-', cylinder_mean).astype(int)
print("Data Types of automobiles (after cylinder mean replacement):\n",
      automobile_df.dtypes)

# Now deal with the cleaning process on type Object (as in displacement column)
# coerce option just means invalid numerica valus coerced to Nans
automobile_df['Displacement'] = pd.to_numeric(automobile_df['Displacement'], errors = 'coerce')
# is now a float 64
print("Data Types of automobiles (after displacement to numeric and coerced):\n",
      automobile_df.dtypes)

# repeat for weight and acceleration columns
automobile_df['Weight'] = pd.to_numeric(automobile_df['Weight'], errors = 'coerce')
automobile_df['Acceleration'] = pd.to_numeric(automobile_df['Acceleration'], errors = 'coerce')
print("Data Types of automobiles (after weight/acceleration to numeric and coerced):\n",
      automobile_df.dtypes)

# before cleaning up origin. examine the data present
# can see origin is code or country name, other cases have more specific details
print("Head of Origin: ")
print_full(automobile_df['Origin'].head(15))

# invoke unique function to see sample of types of data present in this field
print("Unique data types Field 'Origin': \n", automobile_df['Origin'].unique())

# we will use just the Country of Origin.
# It seems the most complete piece of information in all rows
# just US:
# find where any of the Origin contains US, if it does, replace with just US,
# otherwise leave unchanged
automobile_df['Origin'] = np.where(automobile_df['Origin'].str.contains('US'),'US',automobile_df['Origin'])
print("Unique data types field 'Origin' (after replacing for US) :\n ",
      automobile_df['Origin'].unique())

# Japanese:
automobile_df['Origin'] = np.where(automobile_df['Origin'].str.contains('Japan'),'Japan',automobile_df['Origin'])
print("Unique data types field 'Origin' (after replacing for Japan) :\n ",
      automobile_df['Origin'].unique())
#Europe
automobile_df['Origin'] = np.where(automobile_df['Origin'].str.contains('Europe'),'Europe',automobile_df['Origin'])
print("Unique data types field 'Origin' (after replacing for Europe) :\n ",
      automobile_df['Origin'].unique())

# Describing/Exploring Numeric Features:

# first describe function
print("Describe function: ")
print_full(automobile_df.describe())

# SAVE Cleaned Data
automobile_df.to_csv('datasets/cars_processed_08032021.csv', index=False)