#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 10 3 2021
#UpD:
#Descri: learning basics of cleaning and importing data. Demo 2

### Read Clean Explore DataSets

# Resources
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html?highlight=plot#pandas.DataFrame.plot

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

# Imports
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

##  Read In Data

# about cars data set: was modified and purposefully has missings
# etc for this module exercise
automobile_clean_df = pd.read_csv('datasets/cars_processed_08032021.csv')

## Visualization

# fig1 = plt.figure(figsize=(12, 8))
#
# plt.bar(automobile_clean_df['Age'], automobile_clean_df['MPG'])
#
# fig2 = plt.figure(figsize=(12, 8))
# plt.scatter(automobile_clean_df['Acceleration'], automobile_clean_df['MPG'],color='g')
#
# fig3 = plt.figure(figsize=(12,8))
# plt.scatter(automobile_clean_df['Weight'], automobile_clean_df['MPG'], color='b')
#
# fig4 = plt.figure()
# automobile_clean_df.plot.scatter(x='Weight',
#                                  y='Acceleration',
#                                  c='Horsepower', # c is for color
#                                  colormap='viridis',
#                                  figsize=(12,8))

# fig5 = plt.figure()
# plt.bar(automobile_clean_df['Cylinders'], automobile_clean_df['MPG'])

## More Data processing

# drop these b/c they are discrete and not continuous/numeric
# do this before viewing correlation matrix
automobile_clean_df.drop(['Cylinders', 'Origin'], axis=1, inplace=True)

print_full(automobile_clean_df.sample(10))

cars_corr = automobile_clean_df.corr()
print_full(cars_corr)

## Visualization part 2

# visuals w a heat map
fig, ax = plt.subplots(figsize = (12,8))
sns.heatmap(cars_corr, annot = True)

# PLT Controls

plt.xlabel('Cylinders')
plt.ylabel('Miles per gallon')

# execute
plt.show()



