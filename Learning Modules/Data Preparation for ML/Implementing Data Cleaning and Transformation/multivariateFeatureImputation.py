#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 11 3 2021
#UpD:
#Descri: learning basics of imputing. Demo 5

### Read Clean Explore DataSets

# Resources

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

# Imports
import pandas as pd
import numpy as np
# have to use enable iterative imputer because experimental kits need to be
# explicitly enabled
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

# fits regression model on all other features to fill in a missing value.
# this is a machine learning technique

## Data Loading
diabetes_incomplete = pd.read_csv('datasets/diabetes_processed_incomplete.csv')

# instantiate the iterative imputer object
# random_state = 0 is "seeding"
imp = IterativeImputer(max_iter=100, random_state=0)

## First use a toy case
# make fake data to illustrate the example
# each subarray is a C*pow(2)
features = [[4, 2, 1],
            [24, 12, 6],
            [8, np.nan, 2],
            [28, 14, 7],
            [32, 16, np.nan],
            [600, 300, 150],
            [np.nan, 60, 30],
            [np.nan, np.nan, 1]]

# perform the fit using the features
imp.fit(features)

# use this to see how missing values were filled in
print("Transformed, Multivariate Imputed values: \n",
      imp.transform(features))

# Now do another test data set
# this has even more missing values
X_test = [[np.nan, 24, 12],
          [36, np.nan, np.nan],
          [100, np.nan, 25],
          [np.nan, 6, 3],
          [np.nan, 8, np.nan]]

# no perform the imputing using the previously trained model
print("Using X-test data, do Imputing: \n",
      imp.transform(X_test))

## Now use your real data
print("Diabetes Incomplete")
print_full(diabetes_incomplete.head(10))

# look at missings
print("Diabetes Incomplete")
print(diabetes_incomplete.isnull().sum())

# extract features for prediction
diabetes_features = diabetes_incomplete.drop('Outcome', axis=1)
diabetes_label = diabetes_incomplete['Outcome']

# preview the head
print("diabetes features head: ")
print_full(diabetes_features.head())

# reinstantiate the Iterative Imputer Object
# this is used to fill in the missing values of the features array,
# which is just the insulin with 374 null values
imp = IterativeImputer(max_iter=10000, random_state=0)

# fit/train on the diabetes features
imp.fit(diabetes_features)

diabetes_features_array = imp.transform(diabetes_features)

# diabetes features array shape:  (768, 8)
print("diabetes features array shape: ",
      diabetes_features_array.shape)

# convert diabetes features array to
diabetes_features = pd.DataFrame(diabetes_features_array,
                                 columns=diabetes_features.columns)

# we can see all values for the insulin column have been imputed
print("Diabetes Features Head: ")
print_full(diabetes_features.head())

## Bring Fully cleaned and imputed data together with its classifier
diabetes = pd.concat([diabetes_features,diabetes_label],axis=1)

# see a preview
print("Fully cleaned and imputed data set after concatination:")
print_full(diabetes.head())

print("Missing values check: \n", diabetes.isnull().sum())

# Save this cleaned file
diabetes.to_csv('datasets/diabetes_processed.csv', index=False)

