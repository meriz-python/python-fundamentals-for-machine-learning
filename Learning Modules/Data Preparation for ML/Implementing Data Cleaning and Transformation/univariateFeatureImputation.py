#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 10 3 2021
#UpD:
#Descri: learning basics of imputing. Demo 4

### Univariate imputing

# Resources

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('disp lay.max_rows')

# Imports
import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer


## Data Load

# use diabetes dataset b/c outcome is classification (has diabetes or does not)
diabetes = pd.read_csv('datasets/diabetes.csv')

## Exploration

# head
print_full(diabetes.head(10))

# shape:  (768, 9)
print("shape: ", diabetes.shape)

# info: gives quick overview of data, including non-null values, Dtype etc
diabetes.info()

# describe
# note: there are many 0 values in the data set for values which
# should not be like glucose, and blood pressure. look at mins in describe()
# this means missing values are represented as 0s
print("Describe: \n")
print_full(diabetes.describe().transpose())

## Clean data

# replace 0's for missing as NaNs
diabetes['Glucose'].replace(0, np.nan, inplace = True)
diabetes['BloodPressure'].replace(0, np.nan, inplace = True)
diabetes['SkinThickness'].replace(0, np.nan, inplace = True)
diabetes['Insulin'].replace(0, np.nan, inplace = True)
diabetes['BMI'].replace(0, np.nan, inplace = True)

# now evaluate how many values appear missing
# SkinThickness, Insulin have many missings
print("Null values in diabetes: \n", diabetes.isnull().sum())

## Clean SkinThickness column via imputation

# use imputation because we don't have that many values in the dataset

# reshape as 2D array
arr = diabetes['SkinThickness'].values.reshape(-1,1)
print("Array Shape: ", arr.shape)

# use simpleImputer
# impute with mode of non missing values
# this first part instantiates the object, specifying missing values, and the strategy
imp = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
imp.fit(diabetes['SkinThickness'].values.reshape(-1,1))
#now reset onto the original dataset
diabetes['SkinThickness'] =  imp.transform(diabetes['SkinThickness'].values.reshape(-1,1))

# re-explore data

# imputing will change data characteristics
# the mean before 20.536458, sdev = 15.952218
# the mean is now 29.994792, sdev = 8.886506
print("Current Data characteristics")
print_full(diabetes['SkinThickness'].describe())

# evaluate isnulls
print("IsNull: \n", diabetes.isnull().sum())

## impute missing values: feature = Glucose

# apply median strategy
# instantiate simple Imputer object
imp = SimpleImputer(missing_values=np.nan, strategy='median')

# fit the values with simple imputer object methods
imp.fit(diabetes['Glucose'].values.reshape(-1,1))

# transform the fitted values to be placed in the diabetes dataframe
diabetes['Glucose'] = imp.transform(diabetes['Glucose'].values.reshape(-1,1))

# re-explore Glucose data

# old mean = 120.894531 old sdev = 31.972618
# new mean = 121.656250 new sdev = 30.438286
# imputation has mildly changed both of these values
print("Describe Glucose: \n", diabetes['Glucose'].describe())

# see null sums for all data
print("Is Null Sums: \n", diabetes.isnull().sum())

# Now move to blood pressure column

# instantiate new SimpleImputer object
imp = SimpleImputer(missing_values=np.nan, strategy='mean')
imp.fit(diabetes['BloodPressure'].values.reshape(-1,1))

diabetes['BloodPressure'] = imp.transform(diabetes['BloodPressure'].values.reshape(-1,1))

# re-explore change in statistical properties
# old mean 69.105469   old std  19.355807
# new mean 72.405184   new std  12.096346
print("Describe re-printed: ", diabetes['BloodPressure'].describe())


# Now move on to BMI with a mean of 32 (non-constant values).
imp = SimpleImputer(missing_values=np.nan, strategy='constant', fill_value=32)
imp.fit(diabetes['BMI'].values.reshape(-1,1))

diabetes['BMI'] = imp.transform(diabetes['BMI'].values.reshape(-1,1))

# describe w BMI
#  old mean 31.992578  old std  7.884160
# new mean 32.450911  new std 6.875366

print("describe BMI: \n", diabetes['BMI'].describe())

# Now Re-check missing values
# only missing is the insulin column
print("Missings: \n",diabetes.isnull().sum())

# Save the file
diabetes.to_csv('datasets/diabetes_processed_incomplete.csv',index=False)



