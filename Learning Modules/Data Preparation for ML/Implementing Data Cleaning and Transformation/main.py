#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 8 3 2021
#UpD: 11 3 2021
#Descri: learning basics of cleaning and importing data. Demo 1

### Read Clean Explore DataSets

# Imports:
# Libraries
import sklearn
import pandas as pd
import numpy as np

# print some data on software versions
print("sklearn version: ", sklearn.__version__) #0.24.1
print("numpy version: ", np.__version__) #1.20.1

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

##  Call in Support Scripts
# import cleaningData
# import visualizingData
# import buildingRegressionModel
# import univariateFeatureImputation
# import multivariateFeatureImputation
# import missingIndicator
import FeatureImputationPipeline