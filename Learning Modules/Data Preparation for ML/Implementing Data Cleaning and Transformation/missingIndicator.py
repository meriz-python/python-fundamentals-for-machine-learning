#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 23 3 2021
#UpD:
#Descri: learning basics of imputing. Demo 6

### Read Clean Explore DataSets

# Resources

import pandas as pd
import numpy as np

from sklearn.impute import MissingIndicator;

# Step 1:
# useful to have indicaction of exising missing values

# Step 2: given some made-up data
# -1 used as missing values
features = [[4, 2, 1],
            [24, 12, 6],
            [8, 4, 2],
            [28, 14, 7],
            [32, 16, -1],
            [600, 300, 150],
            [-1, 60, 30],
            [-1, 4, 1]]

# Step 3: instantiate MissingIndicator object
indicator = MissingIndicator(missing_values=-1);

# Step 4: Mark the missing values with binaries
# fit_transform method from indicator object
# fit_transform only returns columns where missing values are found.
# this why we see only 2 cols returned from a 3 col matrix
mark_missing_values_only = indicator.fit_transform(features);
print(mark_missing_values_only);

# Step 5: show which features have missing values
# shows feature 0 and 2 have miss values
print(indicator.features_);

# Step 6: Mark missing values w binaries part 2
# instantiate missing indicator object w option
# features = "all"
# to get matrix representation of all features including those w
# no missing values
indicator = MissingIndicator(missing_values=-1, features="all");
mark_all = indicator.fit_transform(features);
print(mark_all)
# we can see all features are included
print(indicator.features_);




