#Author: Jaime Lopez-Merizalde, Ph.D.
#Date: 25 2 2021
#UpD: 28 2 2021
#Descri: learning numpy and pandas etc


# imports
import array
import warnings

import numpy
import pandas
from matplotlib import pyplot as plt

from sklearn.model_selection import train_test_split #train/test split
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression, ElasticNet, Lasso, Ridge
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR #support vector regression
from sklearn.tree import DecisionTreeRegressor #regressing with decision trees

#some configurations
warnings.filterwarnings('ignore') #will not show ignore warning
pandas.set_option('display.max_columns', None)
pandas.set_option('display.max_rows', None)

# FILENAMES
filename = 'dataset/forestfires.csv'

# FILE LOADING
# row names
names = ['X','Y','month','day','FFMC','DMC',
         'DC','ISI','temp','RH','wind','rain',
         'area']

df = pandas.read_csv(filename, header=0, names=names)

# custom data for data train test split and KFold
X, y = numpy.arange(10).reshape((5,2)), range(5)

print("--X--")
print(X)
print("--y--")
print(y)

# Data Pipeline
#print(pandas.isnull(df))

print("************Data Shape*********")
print(df.shape)

print("************Data Types*********")
print(df.dtypes)

print("************Inpsect head of data*********")
#Shows the first row
print(df.head(1))

print("************Inpsect head of data after replacement*********")
#Maybe we want to change the values to a numerical state instead
df.month.replace(('jan', 'feb', 'mar', 'apr','may',
                  'jun','jul','aug','sep','oct','nov','dec'),
                 (1,2,3,4,5,6,7,8,9,10,11,12),inplace = True)
df.day.replace(('mon', 'tue', 'wed', 'thu','fri',
                  'sat','sun'),
                 (1,2,3,4,5,6,7),inplace = True)
array = df.values
print("************Prepare Data Set*********")
#split the features from values we desire to estimate
X = array[:, 0:11]
Y = array[:,12]

print("************Descriptive Statistics, Use Describe*********")
#Note: be critical about using this becasue some variables like Month, day are originally categorical data
print(df.describe())

print("************Correlation, Use Corr*********")
# Shows matrix of linear correlation among two variables
# Month and day correlation makes no sense b/c already catagorical data
print(df.corr(method='pearson'))


# region Folds and Scoring
num_folds = 10
seed = 7
scoring = 'max_error'
scoring2 = 'neg_mean_absolute_error' #negative means:
scoring3 = 'r2'
scoring4 = 'neg_mean_squared_error'

# Spot-Check Preliminary algorithms
models = [] #initialize an array for all models created by multiple algorithms
models.append(('LR', LinearRegression()))
models.append(('LASSO', Lasso()))
models.append(('EN', ElasticNet()))
models.append(('Ridge', Ridge()))


models.append(('KNN', KNeighborsRegressor()))
models.append(('CART', DecisionTreeRegressor()))
models.append(('SVR', SVR()))

# Evaluate models and print results
results = []
names = []
for name, model in models:
    kfold = KFold(n_splits=num_folds, random_state=seed, shuffle=True)
    cv_results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
    cv_results2 = cross_val_score(model, X, Y, cv=kfold, scoring=scoring2)
    cv_results3 = cross_val_score(model, X, Y, cv=kfold, scoring=scoring3)
    cv_results4 = cross_val_score(model, X, Y, cv=kfold, scoring=scoring4)
    msg = "%s: max error: %f , mean absolute error: %f, r2: %f, mean squared error: %f" % (name, cv_results.mean()
                                                        ,-cv_results2.mean(),cv_results3.mean(), -cv_results4.mean())
    print(msg)

    #NOTE: Rsq is negative as reported. this is because the mean is doing BETTER than our regression line
    #This is called "spot-checking" the algorithms to see which would be a great fit at the moment